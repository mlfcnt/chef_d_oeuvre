<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container0WEzqqh\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container0WEzqqh/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/Container0WEzqqh.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\Container0WEzqqh\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \Container0WEzqqh\srcApp_KernelDevDebugContainer([
    'container.build_hash' => '0WEzqqh',
    'container.build_id' => '8b82c7ed',
    'container.build_time' => 1564481353,
], __DIR__.\DIRECTORY_SEPARATOR.'Container0WEzqqh');
